import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TemplateBindingComponent } from './components/template-binding/template-binding.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';
import { InputOutputComponent } from './components/input-output/input-output.component';
import { ServiceSharedComponent } from './components/service-shared/service-shared.component';
import { HomeComponent } from './components/home/home.component';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { HeroFormComponent } from './components/input-output/hero-form/hero-form.component';
import { HeroListComponent } from './components/input-output/hero-list/hero-list.component';
import { FormComponent } from './components/service-shared/form/form.component';
import { ListComponent } from './components/service-shared/list/list.component';
import { ApiExampleComponent } from './components/api-example/api-example.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateBindingComponent,
    NgmodelComponent,
    InputOutputComponent,
    ServiceSharedComponent,
    HomeComponent,
    ReactiveFormComponent,
    HeroFormComponent,
    HeroListComponent,
    FormComponent,
    ListComponent,
    ApiExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // IMPORTANT, other modules needed for properly using the App go here
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
