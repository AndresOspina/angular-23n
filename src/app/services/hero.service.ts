import { Injectable } from '@angular/core';

import { IHeroCharacter } from './../interfaces/hero.interface';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  public heroList: IHeroCharacter[];

  constructor() {
    this.heroList = [];
  }

  public getHeroList(): IHeroCharacter[] {
    return this.heroList;
  }

  public setHero(value: IHeroCharacter) {
    this.heroList.push(value);
  }
}
