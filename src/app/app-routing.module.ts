import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ServiceSharedComponent } from './components/service-shared/service-shared.component';
import { InputOutputComponent } from './components/input-output/input-output.component';
import { NgmodelComponent } from './components/ngmodel/ngmodel.component';
import { TemplateBindingComponent } from './components/template-binding/template-binding.component';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { ApiExampleComponent } from './components/api-example/api-example.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'template-binding', component: TemplateBindingComponent },
  { path: 'ngmodel', component: NgmodelComponent },
  { path: 'reactive-form', component: ReactiveFormComponent },
  { path: 'input-output', component: InputOutputComponent },
  { path: 'service-shared', component: ServiceSharedComponent },
  { path: 'api-example', component: ApiExampleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
