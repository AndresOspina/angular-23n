import { Component, OnInit, Input } from '@angular/core';

import { IHeroCharacter } from './../../../interfaces/hero.interface';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.scss']
})
export class HeroListComponent implements OnInit {
  @Input() heroes: IHeroCharacter[];

  constructor() { }

  ngOnInit() {}
}
